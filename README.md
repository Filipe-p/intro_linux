# OS and Linux Intro Class

This class we'll look at OS systemes and mostly Linux. 


### OS 

OS stands for Operating System. They are a level of abstraction on the bare metal. 

Some have GUI (grafical user interface) such as Android, Microsft, MacOS and lot's of Linux distributions. 

Others do not. 

Some are paid, orther are open source. 

We will be usine Linux a lot! :D 

### Linux

There are many linux distributuiions. Here are some:

- ubuntu (debian) (user friendly, widly used)
- Fedora
- RedHad distros (RHEL)
- Arch 
- Kali (security testing and penetration testing / Ethical hacking)

#### Bash and shells

Linux comes with Bash terminals, and they might have different shells. 

Different shell behale slighly different.

- bash
- oh-myz-sh
- gitbash
- other 

**Small differences in some commands**

#### Package managers

performed through the package manager

- RHEL = yum, dnf or rpm
- Debian = apt-get, aptitude or dpkg

These help install software - Such as python or nginx and apache. 


#### Aspects

Everything is a file
- everything can interact with everything - this is great for automation

#### 3 file desxfiptiors

To iteract with programs and with files. We can usr, redirect and play around with 0, 1 and 2 that are the stdin, stdout, stderr

**0 is Standrad input (stdin)**

**1 is Standard output (stdout)**

**2 is Standard error (stderr)**


### Two Important Hard paths `/` and `~`

`/` - means root directory 

`~`- means `/users/your_login_user/`


### Environment variables

Environment is a place where code runs. 
In bash we might want to set some variables. 

Variable is like a box, you have a name and you add stuff to it. You can open it later. 

In bash we define a variable simply by:

```bash 

# setting a variable
MY_VAR="This is a VAR in my code"

# call the variable using echo $
echo $MY_VAR
> "This is a VAR in my code"

# You can re assing the variable
MY_VAR=4788484
echo $MY_VAR
> 4788484

# Call other variables the same way
echo $USER
> filipepaiva
```

What happens if I close my terminal or open a new one? 

Once you close your terminal session, all variable not in the path will be lost.

#### Child process

A Process that is iniciated under another process, like runing another bash script. 

It goes via the $PATH but is a new "terminal" essentially. 

Hence if we:

```bash
# 1) set variable in terminal
LANDLORD="Jess"

# run the bash script that has echo $LANDLORD
./bash_file.sh
> hi from the file
> 
> Landlord above^^^

# This happens because the ./bash_file.sh runs as a child process - A new terminal.

# also you didn't export
export LANDLORD="Jess"
./bash_file.sh
> hi from the file
> 
> Landlord above^^^

# however, when you turn off your terminal will loose the variable. 

```

To make a Variable Available to a child process you need to `export`. 


### Path

Terminal & bash process follow the PATH

There is a PATH for login users that have a profile and the ones without. 

Files to cosnider to type in variables:  
```bash
# these always exist at location ~
> .zprofile
> .zshrc

# for bash shell without oh-my-zsh:
> .bashrc
> .bash_profile
> .bashprofile 

```

#### Setting & Defining Environment Variables

You need to add them to a file on the path and export them.

### Common Commands

```bash

# check variable on terminal 
$ ENV


# Changeing permisiion 

$ chmod +rwx <files>

```


### Permission 


### Users and Sudoes 


## Wild cards 

### Matchers

You can use this to help you match files or content in files. 

```
# any number of character to a side 
ls exam*
> example     example.jpg example.txt example2

ls *exam
> this_is_an_exam

# ? for specific number of charecters
ls example????
> example.jpg example.txt

## List of specific characters
### each one of these represents 1 character [a-z]
ls example.[aeiout][a-z][a-z]
> example.txt
```

### Redirects 

Every command has 3 defuats.

- stdin - Represent 0
- stdout - Represent 1
- stderr - Represent 2

example of stdout:
```bash
ls
> README.md       example         example.txt     notes (...)
## the list that prints is the stdout!
```

example giving `ls` a stdin - the example????

```bash 
# the example???? is the stdin
ls example????
> example.jpg example.txt
# the output is stdout
```

If there is an error, you get a stderr:
```bash
ls example???
> zsh: no matches found: example???
# the above is a stderr
```

The cool thing is you can redirect this! 

##### > and >>

This `>` will redirect and truncate.

This `>>` will redirect and append. 

Use it with number to decided what to redirect.

#### messing with STDIN 

`<`

### Pipes 

Piping redirect and makes it stdin of another program 

| 


#### GREP 


#### & Sending things to the background 